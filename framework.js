/** Roll your own framework. */

function Model(init, listener) {

	var model = {};

	function notify() {
		if(listener && typeof listener === 'function') {
			listener(model);
		}
	}

	this.update = function(patch) {
		var shouldNotify = false;
		for(var key in patch) {
			if(model[key] !== patch[key]) {
				model[key] = patch[key];
				shouldNotify = true;
			}
		}
		if(shouldNotify) {
			notify();
		}
	};

	this.get = function(key) {
		return model[key];
	};

	this.set = function(key, val) {
		if(model[key] !== val) {
			model[key] = val;
			notify();
		}
	};

	this.model = function() {
		return model;
	};

	this.update(init);
}
