
var blackChar = '\u25A0';
var whiteChar = '\u25A1';

var defaultUrl = 'Philippe-Halsman6.jpg';
//'http://www.edvardmunch.org/images/paintings/the-scream.jpg';

function appendText(area, text) {
  var counter = 0;

  var row = $('<div class="ascii-image-row">');

  var interval = window.setInterval(function() {
    if(!text || counter >= text.length) {
      window.clearInterval(interval);
      return;
    }

    var char = text[counter];
    row.append('<span>' + char + '</span>');
    counter++;
  }, 0);

  area.append(row);

}

function appendRow(area, rowData) {

  var row = $('<div class="ascii-image-row play">');

  var textData = '';
  rowData.forEach(function(item) {
  	textData += item;
  });

  row.append(document.createTextNode(textData));

  area.append(row);
}

var theImage = $('#image');

theImage.on('load', renderAsciiImage);

function renderAsciiImage() {

	console.log('rendering...');
	$('#area').empty();

	// Returns the Moore neighbourhood to the nth degree for the given index.
	function getMooreN(idx, n) {
		var w = imageSize.width * itemsPerPixel;
		var pixLen = 4;
		var leftEdge = (idx - (pixLen * n)) % w;
		var rightEdge = (idx + (pixLen * n)) % w;
		var topEdge = idx - (n * w) - (idx % w);
		var bottomEdge = idx + (n * w) - (idx % w);
		var mooreN = [];
		for(var rowIdx = topEdge; rowIdx < bottomEdge; rowIdx += w) {
			for(var colIdx = leftEdge; colIdx < rightEdge; colIdx += pixLen) {
				var mooreIdx = rowIdx + colIdx;
				mooreN.push(mooreIdx);
			}
		}
		return mooreN;
	}

	function getAsciiWidth() {
		var pixelWidth = $('#area').width();
		console.log('area width', pixelWidth);
		
		var testCase = $('<span>').text(blackChar).css({position: 'absolute', left: -1000, padding: 0 }).addClass('ascii-image-row');
		$(document.body).append(testCase);

		var charWidth = testCase.width();
		console.log('charWidth', charWidth);
		testCase.remove();

		return Math.floor(pixelWidth / charWidth);
	}

	var imageSize = { width: this.naturalWidth, height: this.naturalHeight };

	var backingCanvas = document.createElement('canvas');
	backingCanvas.width = imageSize.width;
	backingCanvas.height = imageSize.height;

	var backingContext = backingCanvas.getContext('2d');
	backingContext.drawImage(theImage[0], 0, 0, imageSize.width, imageSize.height);

	var idata = backingContext.getImageData(0, 0, imageSize.width, imageSize.height);

	console.log('put', idata);

	var rawImageData = idata.data;
	var asciiImageData = [];

	var threshold = 100;
	var spread = 255 / 2;

	for(var i = 0; i < rawImageData.length; i += 4) {
		var grey = (rawImageData[i] + rawImageData[i+1] + rawImageData[i+2]) / 3;
		var thresholdTest = Math.ceil(grey / spread) * spread;

		for(var j = 0; j < 3; j++) {
			var idx = i + j;
			rawImageData[idx] = Math.ceil(rawImageData[idx] / spread) * spread;;
		}
	}

	var context2d = $('#c')
		.attr('width', imageSize.width)
		.attr('height', imageSize.height)
		.css({ width: theImage.width(), height: theImage.height() })
		[0].getContext('2d');

	context2d.scale(theImage.width() / imageSize.width, theImage.height() / imageSize.height);
	context2d.putImageData(idata, 0, 0);

	var asciiImageWidth = getAsciiWidth();
	var pixelsPerChar = Math.floor(imageSize.width / asciiImageWidth);
	var itemsPerPixel = 4;
	var itemsPerChar = itemsPerPixel * pixelsPerChar;
	var itemsPerRow = itemsPerPixel * imageSize.width;

	for(var rowIdx = 0; rowIdx < rawImageData.length; rowIdx += itemsPerRow * pixelsPerChar) {
		for(var i = 0; i < itemsPerChar * asciiImageWidth; i += itemsPerChar) {
			var idx = rowIdx + i;
			var total = 0;
			var pixelsToConsider = getMooreN(idx, 2);
			for(var j = 0; j < pixelsToConsider.length; j++) {
				total += rawImageData[pixelsToConsider[j]];
			}
			var average = total / pixelsToConsider.length;
			asciiImageData.push(Math.ceil(average / spread));
		}
	}

	console.log('imageSize', imageSize);
	console.log('theImage width/height', theImage.width(), theImage.height())
	console.log('ascii data length', asciiImageData.length);
	console.log('pix per char', pixelsPerChar);
	console.log('ascii width', asciiImageWidth);
	console.log('items per row', itemsPerRow);
	console.log('items per char', itemsPerChar);

	for(var i = 0; i < asciiImageData.length; i += asciiImageWidth) {
		var row = asciiImageData.slice(i, i + asciiImageWidth);
		appendRow($('#area'), row);
	}
}

/** Handle Url change. */
function onInputChanged(inputModel) {
	console.log('input changed, updating...');
	blackChar = inputModel.aChar;
	whiteChar = inputModel.bChar;
	theImage.attr('src', 'test.php?url=' + inputModel.url);
}

var urlInputModel = new Model(
	{
		url: defaultUrl, 
		aChar: blackChar, 
		bChar: whiteChar 
	}, 
	onInputChanged);

$('#aChar').val(urlInputModel.model().aChar);
$('#bChar').val(urlInputModel.model().bChar);
$('#imageSelection').val(urlInputModel.model().url);

$('#submit').click(function() {
	urlInputModel.update(
	{ 
		url: $('#imageSelection').val(),
		aChar: $('#aChar').val(),
		bChar: $('#bChar').val()
	});
});

